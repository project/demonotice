<?php


function demonotice_settings_form($node, &$form_state) {
  $form['enabled'] = array(
    '#type'          => 'radios', 
    '#title'         => t('Status'), 
    '#default_value' => variable_get('demonotice_settings_enabled', 1),
    '#options'       => array(t('Disabled'), t('Enabled')),
  );
  $form['position'] = array(
    '#type'          => 'radios', 
    '#title'         => t('Notice position'), 
    '#default_value' => variable_get('demonotice_settings_position', 1),
    '#options'       => array(t('Header'), t('Footer')),
  );
  $form['enableadmin'] = array(
    '#type'          => 'radios', 
    '#title'         => t('Enlable notice for admin'), 
    '#default_value' => variable_get('demonotice_settings_enableadmin', 1),
    '#options'       => array(t('No'), t('Yes')),
  );
  $form['caption'] = array(
    '#type'          => 'text_format', 
    '#title'         => t('Demo notice message'), 
    '#default_value' => variable_get('demonotice_settings_caption', t('Notice this website is currently in development, Things could change at any time.')), 
    '#wysiwyg '      => true,
    '#required'      => TRUE, 
  );

  $form['submit'] = array(
   '#type'           => 'submit',
   '#value'          => t('Save')
  );
  return $form;	
}


function demonotice_settings_form_submit($node, &$form_state) {
  $values      = $form_state['values'];
  $enabled     = $values['enabled'];
  $position    = $values['position'];
  $caption     = $values['caption'];
  $enableadmin = $values['enableadmin'];
   
  variable_set('demonotice_settings_caption',  $caption['value']);
  variable_set('demonotice_settings_position', $position);
  variable_set('demonotice_settings_enabled',  $enabled);
  variable_set('demonotice_settings_enableadmin', $enableadmin);
  
  drupal_set_message(t('Demonotice settings saved.'));
}
