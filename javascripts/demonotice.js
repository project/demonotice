function demonotice_position_top_bar() {
  jQuery('.toolbar-processed').each(function(index, obj) {
	  var height = jQuery(this).height();
	  jQuery('.demonnotice_notice.header').css('top', height+'px');
  });	
}

jQuery(document).ready(function() {
	demonotice_position_top_bar();
	jQuery('#toolbar a.toggle').click(function() {
		demonotice_position_top_bar();
	});
});
